import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;



public class login {
    public static WebDriver driver;

    public static void main(String[] args) throws IOException, ParseException {
        FileInputStream fileInput = new FileInputStream("/Users/shahnawaz/IdeaProjects/webautomation/src/property/data.Properties");
        Properties prop = new Properties();
        prop.load(fileInput);

        if(prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();


        }
        else {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();


        }

        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("/Users/shahnawaz/IdeaProjects/webautomation/src/datasignup/signup_data.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignUpData");

        JSONObject userDetail = (JSONObject) array.get(0);
        driver.get(prop.getProperty("url"));
        HomePage hp = new HomePage(driver);
        hp.signIn();

        Signup su = new  Signup(driver);

        su.EnterEmail2((String) userDetail.get("email_create"));
        su.EnterPassword((String) userDetail.get("passwd"));
        su.SubmitlogIn();
        DashbBoar d = new DashbBoar(driver);
        d.woman();
        d.morebtnclk();
        Add_to_cart ac = new Add_to_cart(driver);
        ac.qnt();
        ac.sze();
        ac.add();


    }
}
