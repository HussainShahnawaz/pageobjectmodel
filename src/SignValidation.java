import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SignValidation {



    public static WebDriver driver;

    public static void main(String[] args) throws IOException, ParseException {
        FileInputStream fileInput = new FileInputStream("/Users/shahnawaz/IdeaProjects/webautomation/src/property/data.Properties");
        Properties prop = new Properties();
        prop.load(fileInput);

        if(prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();


        }
        else {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();


        }

        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("/Users/shahnawaz/IdeaProjects/webautomation/src/datasignup/signup_data.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignUpData");

        JSONObject userDetail = (JSONObject) array.get(0);
        driver.get(prop.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        HomePage hp = new HomePage(driver);
        hp.signIn();

        Signup su = new  Signup(driver);
        su.enteremail((String) userDetail.get("email_create"));
        su.SubmitCreate();
        user_register ur = new user_register(driver);
        ur.checkgndr();
        ur.firstName((String) userDetail.get("customer_firstname"));
        ur.lastName((String) userDetail.get("customer_lastname"));
        ur.password((String) userDetail.get("passwd"));
        ur.dayss();
        ur.monthh();
        ur.year();
        ur.company((String) userDetail.get("company"));
        ur.address((String) userDetail.get("address1"));
        ur.city((String) userDetail.get("city"));
        ur.stat();
        ur.pscode((String) userDetail.get("postcode"));
        ur.phnno((String) userDetail.get("phone_mobile"));
        ur.submitdet();



    }
}



