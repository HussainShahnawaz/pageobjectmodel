import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class user_register extends pageInit {


    public user_register(WebDriver driver) {

        super(driver);
    }

    @FindBy(id= "id_gender1")
    WebElement gender;


    @FindBy(id= "customer_firstname")
    WebElement fname;

    @FindBy(id= "customer_lastname")
    WebElement lname;

    @FindBy(id= "days")
    WebElement bday;

    @FindBy(id= "months")
    WebElement bmonth;

    @FindBy(id= "years")
    WebElement byear;

    @FindBy(id= "company")
    WebElement mcompany;

    @FindBy(id= "address1")
    WebElement add1;

    @FindBy(id= "city")
    WebElement mcity;

    @FindBy(id= "id_state")
    WebElement state;

    @FindBy(id= "postcode")
    WebElement pincode;

    @FindBy(id= "phone_mobile")
    WebElement mphn;

    @FindBy(id= "passwd")
    WebElement pwd;

    @FindBy(id = "submitAccount")
    WebElement submitbtn;

    public void checkgndr()
    {

        gender.click();
    }

    public void firstName(String frstnm){
        fname.sendKeys(frstnm);
    }

    public void lastName(String lstnm){
        lname.sendKeys(lstnm);
    }

    public void dayss(){
        Select dropdown = new Select(bday);
        dropdown.selectByIndex(2);
    }

    public void monthh(){
        Select dropdown = new Select(bmonth);
        dropdown.selectByIndex(11);
    }

    public void year() {
        Select dropdown = new Select(byear);
        dropdown.selectByValue("1998");
    }

    public void company(String comp){
        mcompany.sendKeys(comp);
    }

    public void address(String addrs){
        add1.sendKeys(addrs);
    }

    public void city (String cty){
        mcity.sendKeys(cty);
    }
    public void stat(){
        Select dropdown3 = new Select(state);
        dropdown3.selectByVisibleText("Indiana");

    }

    public void pscode (String pscd){
        pincode.sendKeys(pscd);


    }


    public void phnno (String phon){

        mphn.sendKeys(phon);
    }

    public void password (String pswd){
        pwd.sendKeys(pswd);
    }

    public void submitdet (){
        submitbtn.click();
    }
}



